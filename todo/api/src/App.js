import React, { Component } from "react";
import axios from 'axios';
import './App.css';

class App extends Component {

  constructor(props){
      super(props);

      this.state = {
          isLogin:false,
          departmentList : [],
          user:null,
          search:"",
          searchResult: [],
          resultCount: "",
          resultList: [],
      };
  }

  componentDidMount() {
      this.httpClient = axios.create({
          baseURL:'https://kadou.i.nijibox.net/api',
          withCredentials:true,
      });


      console.log(this);
      this.loadAuth()
          .then(()=>{
              if(! this.state.isLogin){
                  return Promise.resolve();
              }
              return this.loadDepartments();
          })
          .catch((err)=>{
              alert("APIがエラーを返しました\n\n" + err);
          })

      ;
  }
  loadAuth(){
      return this.httpClient.get('/auth' , {params:{callback:'http://localhost:3000'}})
          .then(this.commonResponseHandling)
          .then((result)=>{
              if(result.is_login){
                  this.setState({isLogin:true});
              }else if(result.auth_url){
                  window.location.href = result.auth_url;
              }
          });
  }
  loadDepartments(){
      return this.httpClient.get('/who/departments')
          .then(this.commonResponseHandling)
          .then((result)=>{
              this.setState({departmentList : result});
          })
  }
  loadUser(){
      return this.httpClient.get('/who/search', {params:{query:this.state.search}})
        .then(this.commonResponseHandling)
        .then((result)=>{
            this.setState({
                searchResult: result.item_list,
                resultCount: result.item_list.length
            });
            // this.searchData();
        })
    }
    // searchData(){
    //     console.log(this.state.resultCount);
    //     for(var i=0; i<this.state.resultCount; i++) {
    //         this.httpClient.get('/who/user/' + this.state.searchResult[i].user_id)
    //             .then(this.commonResponseHandling)
    //             .then((result)=>{
    //                 console.log(result.user_name);
    //                 // this.state.resultList.push(
    //                 //     <li key={result.user_id}>{ result.user_name }</li>);
    //             })
    //     }
    // }

  commonResponseHandling(res){
      console.debug(res);
      if(res.data.code !== "200"){
          console.error(res.data.data);
          return Promise.reject("API Error:" + res.data.data.message);
      }
      return Promise.resolve(res.data.data);
  }
  
//   clickHandler(){
//       console.log(this);
//       this.loadUser()
//           .catch((err)=>{
//               alert('エラー発生');
//           });
//   }
  handleChange(e) {
    this.setState({search: e.target.value});
  }

  /**
   * Reactの仮想DOMを作る命令です。
   */
  render() {
      const resultList = [];
      if(this.state.resultCount !== 0) {
        for(var i=0; i<this.state.resultCount; i++) {
            resultList.push(
                <tr key={this.state.searchResult[i].user_id}>
                    <td>{this.state.searchResult[i].user_id}</td>
                    <td>{this.state.searchResult[i].user_name}</td>
                    <td>{this.state.searchResult[i].mail}</td>
                    <td>{this.state.searchResult[i].department_name}</td>
                    <td><img src={this.state.searchResult[i].photo_url} alt={this.state.searchResult[i].user_id} className="photo" /></td>
                </tr>);
        }
      } else {
        resultList.push(
            <tr><td key="0">ヒットしませんでした</td></tr>);
      }
    

    // 仮想DOMを作成する
    return (
        <div className="wrapper">
            <h1 className="title">気になるあの子を見つけ隊</h1>
            { this.state.isLogin ?
                <div>
                    <input type="text" value={this.state.search} onChange={e => this.handleChange(e)} />
                    <button onClick={(e) => this.loadUser(e)}>ユーザを検索</button>

                    <table>
                        <tbody>
                            <tr><th>ユーザーID</th><th>名前</th><th>メールアドレス</th><th>所属</th><th>顔写真</th></tr>
                            {resultList}
                        </tbody>
                    </table>
                </div>
                :
                <p>ログインしてください</p>
            }
        </div>

    );
  }
}

export default App;
