import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

// ReactをHTMLのdivタグ(id="root")にレンダリングする
ReactDOM.render(<App />, document.getElementById("root"));
