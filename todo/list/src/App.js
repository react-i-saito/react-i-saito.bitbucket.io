import React, { Component } from "react";
import axios from 'axios';
import './App.css';

class App extends Component {

  constructor(props){
      super(props);

      this.state = {
          isLogin:false,
          departmentList : [],
          user:null,
          search:"",
          searchResult: [],
          resultCount: "",
          userData: [],
          detailDom: "",
          flgClose: false,
          page: [],
          displayPage: "1",
          resultTotalCount: "",
          flgUpdate: true,
      };
  }

  componentDidMount() {
      this.httpClient = axios.create({
          baseURL:'https://kadou.i.nijibox.net/api',
          withCredentials:true,
      });

      this.loadAuth()
          .then(()=>{
              if(! this.state.isLogin){
                  return Promise.resolve();
              }
              return this.loadDepartments();
          })
          .catch((err)=>{
              alert("APIがエラーを返しました\n\n" + err);
          })

      ;
  }
  loadAuth(){
      return this.httpClient.get('/auth' , {params:{callback:'https://react-i-saito.bitbucket.io/todo-build/'}})
          .then(this.commonResponseHandling)
          .then((result)=>{
              if(result.is_login){
                  this.setState({isLogin:true});
              }else if(result.auth_url){
                  window.location.href = result.auth_url;
              }
          });
  }
  loadDepartments(){
      return this.httpClient.get('/who/departments')
          .then(this.commonResponseHandling)
          .then((result)=>{
              this.setState({departmentList : result});
          })
  }
  
  loadUser(){
      return this.httpClient.get('/who/search', {params:{query:this.state.search,page: this.state.displayPage}})
        .then(this.commonResponseHandling)
        .then((result)=>{
            this.setState({
                searchResult: result.item_list,
                resultCount: result.item_list.length,
                page: result.summary,
                resultTotalCount: result.summary.total_items,
            });
        })
  }
  handlePage(e){
    const toPage = e.target.innerText;

    this.setState({
        displayPage: toPage,
        flgUpdate: false,
    }, () => {
        this.loadUser();
    })
  }

  commonResponseHandling(res){
      console.debug(res);
      if(res.data.code !== "200"){
          console.error(res.data.data);
          return Promise.reject("API Error:" + res.data.data.message);
      }
      return Promise.resolve(res.data.data);
  }
  
//   clickHandler(){
//       console.log(this);
//       this.loadUser()
//           .catch((err)=>{
//               alert('エラー発生');
//           });
//   }
  handleChange(e) {
    this.setState({search: e.target.value});
  }
  handleSearch(){
      if(this.state.flgUpdate === false){
        this.setState({
            displayPage: 1,
            flgUpdate: true,
        }, () => {
            this.loadUser();
        })
      } else {
        this.loadUser();
      }
  }
  handleMoreData(e, id) {
    if(this.state.flgClose === true){
        this.setState({flgClose: false});
        return false;
    }
    return this.httpClient.get('/who/user/' + id)
        .then(this.commonResponseHandling)
        .then((result)=>{
            this.setState({
                userData: result,
                flgClose: true
            });
        })
  }

  /**
   * Reactの仮想DOMを作る命令です。
   */
  render() {
      const resultTotalCount = [];
    //   if(this.state.resultCount !== ""){
    //       resultCount.push(
    //         <div key={this.state.resultCount} className="pageCount">{this.state.resultCount}件ヒットしました</div>
    //       );
    //   }
    //   <div className="pageCount">{this.state.resultCount}件ヒットしました</div>
      const resultList = [];
      if(this.state.resultTotalCount !== "" ) {
        if (this.state.resultTotalCount !== 0) {
            resultList.push(
                <tr key="0" className="tableHeader"><th>名前</th><th>メールアドレス</th><th>所属</th><th>顔写真</th></tr>);
            for(var i=0; i<this.state.resultCount; i++) {
                const item = this.state.searchResult[i];
                resultList.push(
                    <tr key={item.user_id} onClick={(e) => this.handleMoreData(e,item.user_id)}>
                        <td>{this.state.searchResult[i].user_name}</td>
                        <td>{this.state.searchResult[i].mail}</td>
                        <td>{this.state.searchResult[i].department_name}</td>
                        <td><img src={this.state.searchResult[i].photo_url} alt={this.state.searchResult[i].user_name} className="photo" /></td>
                    </tr>);
            }
            resultTotalCount.push(
            <div key={this.state.resultTotalCount} className="pageCount">{this.state.resultTotalCount}件ヒットしました（＾▽＾）</div>
            );
        } else {
            resultList.push(
                <tr key="0"><td>ヒットしませんでした（；△；）</td></tr>);
        }
        
      }

    const pageNum = [];
    const totalPage = this.state.page.total_pages;
    if(totalPage > 1) {
        for(var a=1; a<totalPage+1; a++) {
            pageNum.push(
                <li key={a} onClick={(e) => this.handlePage(e)}>{a}</li>);
        }
    }
      
      const detailData = [];
      if(this.state.flgClose) {
        const item = this.state.userData;
        detailData.push(
            <div key={item.user_id} id="modal" className="modal">
                <div className="overlay" onClick={()=>this.handleMoreData()}></div>
                <div className="detail">
                    <ul>
                        <li><span className="detail-item">社員番号：</span>{item.user_code}</li>
                        <li><span className="detail-item">名前：</span>{item.user_name}</li>
                        <li><span className="detail-item">なまえ：</span>{item.user_kana}</li>
                        <li><span className="detail-item">slack表示名：</span>{item.slack_display_name}</li>
                    </ul>
                    <ul>
                        <li><img src={item.main_photo_url} alt={item.user_name} className="photo" /></li>
                        <li><span className="detail-item">メモ：</span>{item.discription}</li>
                    </ul>
                </div>
            </div>);
      }
    

    // 仮想DOMを作成する
    return (
        <div className="wrapper">
            <h1 className="title">気になるあの子を見つけ隊</h1>
            { this.state.isLogin ?
                <div>
                    <input type="text" value={this.state.search} onChange={e => this.handleChange(e)} className="input" />
                    <button onClick={(e) => this.handleSearch(e)} className="button">検索</button>

                    {resultTotalCount}
                    <table>
                        <tbody>
                            {resultList}
                        </tbody>
                    </table>
                    <ul className="pageNum">
                        {pageNum}
                    </ul>
                    {detailData}
                </div>
                :
                <p>ログインしてください</p>
            }
        </div>

    );
  }
}

export default App;
