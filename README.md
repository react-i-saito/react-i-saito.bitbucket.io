# react-i-saito

React開発合宿用のリポジトリ

## 環境
- node.js v11.6.0
- ruby 2.4.1

## 環境
todoディレクトリが作業用。buildディレクトリにコンパイルされたものを
todo-buildディレクトリにまるっとコピーしてweb上に公開しています
https://react-i-saito.bitbucket.io/todo-build/
合宿作業ディレクトリは/todo/list/です
