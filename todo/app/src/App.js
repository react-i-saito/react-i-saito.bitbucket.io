import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(){
    super();
    // ステート
    this.state = {
      inputText : "テスト",
      taskList : [],
      filterType : "all", // all, active, completed
    };
  }

  handleTextChange(event) {
    this.setState({
      inputText : event.target.value
    })
  }

  handleAddClick(event) {
    const taskList = this.state.taskList.concat();
    // 配列に、ステートに保持された入力済み文字を追加
    taskList.push({
      key : Date.now(), // ユニークなキーということで時間を設定
      completed : false, // 完了しているか
      label : this.state.inputText, // タスクの文字
    });
    // 新しいステートを登録
    this.setState({
      taskList : taskList
    })

    console.log(taskList)
  }

  handleListChange(event, key) {
    const checked = event.target.checked;
    console.log(checked, key);

    const taskList = this.state.taskList.concat();
    for(var i=0; i<taskList.length; i++){
      if(taskList[i].key === key){
        taskList[i].completed = checked;
      }
    }

    this.setState({
      taskList : taskList
    })
  }


  render() {
    // mapメソッド
    // 配列の１つ１つを取り出してそれぞれに処理を加えることができる
    // const nodes = this.state.taskList.map(item => {
    //   return <li>{item}</li>
    // })

    // es5に変換
    const nodes = [];
    for(var i=0; i<this.state.taskList.length; i++){

      const item = this.state.taskList[i];
      
      let flag = true;
      if(this.state.filterType === "all"){
      }else if(this.state.filterType === "active"){
        if(item.completed === true) {
          flag = false;
        }
      } else if(this.state.filterType === "completed"){
        if(item.completed === false) {
          flag = false;
        }
      }
      

      if(flag === true) {
      nodes.push(
      <li key={item.key}>
        <label>
          <input type="checkbox"
          checked={item.completed}
          onChange={(e)=>this.handleListChange(e, item.key)}
           />
          {item.label}
        </label>
      </li>);
      }
    }

    return (
      <div className="App">
        <h1>TODO LIST</h1>
        <div>
          <input type="text"
          onChange={(e) => this.handleTextChange(e)}
          placeholder="タスクを入力してね" />
          <button
          onClick={(e) => this.handleAddClick(e)}>追加</button>
        </div>

        <p>{this.state.inputText}</p>

        <ul>
          {nodes}
        </ul>
        <div>
          <label><input type="radio" value="all"
            onChange={(e) => this.handleFilterChange('all')}
           checked={this.state.filterType === "all"} />全て</label>
          <label><input type="radio" value="active"
            onChange={(e) => this.handleFilterChange('active')}
           checked={this.state.filterType === "active"} />未完了のみ</label>
          <label><input type="radio" value="completed"
            onChange={(e) => this.handleFilterChange('completed')}
           checked={this.state.filterType === "completed"} />完了のみ</label>
        </div>
      </div>
    );
  }
  handleFilterChange(filterType) {
    this.setState({
      filterType: filterType
    })
  }
}

export default App;
